package net.davidculpepper.layoutmaster;

import android.app.Application;

import com.squareup.sqlbrite.BriteDatabase;
import com.squareup.sqlbrite.SqlBrite;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import rx.schedulers.Schedulers;

public class App extends Application {
    private WebApi webApi;
    private BriteDatabase databaseHelper;

    public BriteDatabase getDatabaseHelper() {
        if (databaseHelper == null) {
            databaseHelper = SqlBrite.create().wrapDatabaseHelper(new MainActivity.DatabaseHelper(this), Schedulers.io());
        }
        return databaseHelper;
    }

    public WebApi getWebApi() {
        if (webApi == null) {
            webApi = new Retrofit.Builder()
                    .addConverterFactory(GsonConverterFactory.create())
                    .baseUrl("https://timesfnl.praeses.com")
                    .build()
                    .create(WebApi.class);
        }
        return webApi;
    }
}
