package net.davidculpepper.layoutmaster;

import android.app.Activity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Response;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class WebAccessActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_access);

        final RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        final Adapter adapter = new Adapter(this);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        Observable.just(getApi().getGames(1))
                .subscribeOn(Schedulers.io())
                .map(x -> {
                    try {
                        return x.execute();
                    } catch (IOException ex) {
                        throw new RuntimeException(ex);
                    }
                })
                .map(x -> x.body())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(x -> {
                    adapter.updateGames(x);
                }, ex -> {
                });
    }

    private WebApi getApi() {
        return ((App)getApplication()).getWebApi();
    }

    public static class Adapter extends RecyclerView.Adapter<ViewHolder> {

        private final Activity context;
        private List<WebApi.GameDto> games = new ArrayList<>();

        public Adapter(Activity context) {
            this.context = context;
        }

        public void updateGames(List<WebApi.GameDto> games) {
            this.games = games;
            notifyDataSetChanged();
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = context.getLayoutInflater().inflate(R.layout.list_item_game, parent, false);
            return new ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, int position) {
            WebApi.GameDto game = games.get(position);
            holder.awayScore.setText(String.valueOf(game.score.getAwayScore()));
            holder.homeScore.setText(String.valueOf(game.score.getHomeScore()));
            holder.period.setText(game.score.getPeriod());
        }

        @Override
        public int getItemCount() {
            return games.size();
        }
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        public final TextView awayScore;
        public final TextView homeScore;
        public final TextView period;

        public ViewHolder(View itemView) {
            super(itemView);

            awayScore = (TextView) itemView.findViewById(R.id.awayScore);
            homeScore = (TextView) itemView.findViewById(R.id.homeScore);
            period = (TextView) itemView.findViewById(R.id.period);
        }
    }
}
