package net.davidculpepper.layoutmaster;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;

public class MainActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private Adapter adapter;
    private Subscription query;

//    private Model model;
//    private TextView countView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


//        countView = (TextView) findViewById(R.id.count);
//
//        if (savedInstanceState != null) {
//            model = new Gson().fromJson(savedInstanceState.getString("model"), Model.class);
//        } else {
//            String json = PreferenceManager.getDefaultSharedPreferences(this).getString("model", null);
//            model = json != null ? new Gson().fromJson(json, Model.class) : new Model();
//        }
//        updateView();
//        findViewById(R.id.addButton).setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                model.count++;
//                updateView();
//                save();
//            }
//        });
//
//        findViewById(R.id.subtractButton).setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                model.count--;
//                updateView();
//                save();
//            }
//        });


        setContentView(R.layout.activity_list);

        findViewById(R.id.addButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addItem();
            }
        });

        adapter = new Adapter(this);
        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adapter);

        query = ((App)getApplication()).getDatabaseHelper()
                .createQuery(DbContact.TABLE_NAME, DbContact.SELECT_ALL)
                .mapToList(DbContact.MAPPER::map)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(list -> adapter.updateContacts(list));
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        query.unsubscribe();
        query = null;
    }

    private void addItem() {
        ((App)getApplication()).getDatabaseHelper().insert(DbContact.TABLE_NAME, DbContact.FACTORY.marshal()
                .avatar("https://gravatar.com/avatar/e48c05a62eb28d089779ffe149f1dbfd?s=200")
                .name("Faramir")
                .location("Sitting in a tower")
                .status("Mopy")
                .asContentValues());
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        //outState.putString("model", new Gson().toJson(model));
    }

    public void updateView() {
        //countView.setText(String.valueOf(model.count));
    }

    public void save() {
//        PreferenceManager.getDefaultSharedPreferences(this)
//                .edit()
//                .putString("model", new Gson().toJson(model))
//                .apply();
    }

    public class Model {
        public int count;
    }

    public static class Adapter extends RecyclerView.Adapter<ViewHolder> {

        private final Activity context;
        private List<DbContact> contacts = new ArrayList<>();

        public Adapter(Activity context) {
            this.context = context;
        }

        public void updateContacts(List<DbContact> contacts) {
            this.contacts = contacts;
            notifyDataSetChanged();
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = context.getLayoutInflater().inflate(R.layout.list_item_contact, parent, false);
            return new ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, int position) {
            DbContact contact = contacts.get(position);
            holder.name.setText(contact.name());
            holder.status.setText("Status: " + contact.status());
            holder.location.setText("Location: " + contact.location());

            Picasso.with(context).load(contact.avatar()).into(holder.avatar);
        }

        @Override
        public int getItemCount() {
            return contacts.size();
        }
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        public final ImageView avatar;
        public final TextView name;
        public final TextView status;
        public final TextView location;

        public ViewHolder(View itemView) {
            super(itemView);

            avatar = (ImageView) itemView.findViewById(R.id.image);
            name = (TextView) itemView.findViewById(R.id.name);
            status = (TextView) itemView.findViewById(R.id.status);
            location = (TextView) itemView.findViewById(R.id.location);
        }
    }

    public static class DatabaseHelper extends SQLiteOpenHelper {

        public static final String DB_NAME = "database";
        public static final int DB_VERSION = 3;
        public static final SQLiteDatabase.CursorFactory NULL_FACTORY = null;

        public DatabaseHelper(Context context) {
            super(context, DB_NAME, NULL_FACTORY, DB_VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            db.execSQL(DbContact.CREATE_TABLE);

            for (int i = 0; i < 10; i++) {
                db.insert(DbContact.TABLE_NAME, null, DbContact.FACTORY.marshal()
                        .avatar("https://gravatar.com/avatar/e48c05a62eb28d089779ffe149f1dbfd?s=200")
                        .name("Boromir")
                        .location("Floating down a river")
                        .status("Dead")
                        .asContentValues());
            }
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            db.execSQL("DROP TABLE IF EXISTS " + DbContact.TABLE_NAME);
            onCreate(db);
        }
    }
}