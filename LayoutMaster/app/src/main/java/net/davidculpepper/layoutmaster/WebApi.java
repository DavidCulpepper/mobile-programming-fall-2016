package net.davidculpepper.layoutmaster;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

/**
 * Created by dculpepper on 10/25/2016.
 */

public interface WebApi {

    @GET("/weeks/{weekId}/games.json")
    Call<List<GameDto>> getGames(@Path("weekId") long weekId);

    public static class GameDto {
        public ScoreDto score;
    }

    public static class ScoreDto {
        @SerializedName("home_score")
        private long homeScore;
        @SerializedName("away_score")
        private long awayScore;
        private String period;

        public long getHomeScore() {
            return homeScore;
        }

        public void setHomeScore(long homeScore) {
            this.homeScore = homeScore;
        }

        public long getAwayScore() {
            return awayScore;
        }

        public void setAwayScore(long awayScore) {
            this.awayScore = awayScore;
        }

        public String getPeriod() {
            return period;
        }

        public void setPeriod(String period) {
            this.period = period;
        }
    }
}