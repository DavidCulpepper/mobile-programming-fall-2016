package net.davidculpepper.layoutmaster;

import com.google.auto.value.AutoValue;
import com.squareup.sqldelight.RowMapper;

@AutoValue
public abstract class DbContact implements ContactModel {
    public static final Factory<DbContact> FACTORY = new Factory<>(AutoValue_DbContact::new);
    public static final RowMapper<DbContact> MAPPER = FACTORY.select_allMapper();
}