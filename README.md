### How to Fix your Repository

First, make sure that all changes to your repository have been committed.

Next add the following to the ``.gitignore`` file at the root of your repository:

````
*.iml
.gradle
local.properties
.idea/
.DS_Store
build/
captures/
.externalNativeBuild

````

Now run the following two commands from the git command line:

````
git rm -r --cached .
git add .
git commit -m "Remove ignored files"
git push origin master
````

