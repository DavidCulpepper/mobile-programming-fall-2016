package net.davidculpepper.geoquiz;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.Looper;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class CheatActivity extends AppCompatActivity {

    public static final String QUESTION_KEY = "question";
    public static final String ANSWER_KEY = "answer";

    @BindView(R.id.answer) TextView answer;
    @BindView(R.id.question) TextView question;
    @BindView(R.id.confirmButton) Button confirmButton;

    public static Intent createIntent(Context context, String question, boolean answer) {
        Intent i = new Intent(context, CheatActivity.class);
        i.putExtra(ANSWER_KEY, answer);
        i.putExtra(QUESTION_KEY, question);
        return i;
    }

    public static boolean hasCheated(Intent data) {
        return data.getExtras().getBoolean("hasCheated");
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cheat);
        boolean passedInAnswer = getIntent().getBooleanExtra(ANSWER_KEY, false);
        String passedInQuestion = getIntent().getStringExtra(QUESTION_KEY);

        ButterKnife.bind(this);
        answer.setText(String.valueOf(passedInAnswer));
        question.setText(passedInQuestion);
    }

    @OnClick(R.id.confirmButton) void onConfirmClicked() {
        new Handler(Looper.myLooper()).postDelayed(new Runnable() {
            @Override
            public void run() {
                answer.setVisibility(View.VISIBLE);
                confirmButton.setVisibility(View.INVISIBLE);
            }
        }, 250);
        Intent data = new Intent();
        data.putExtra("hasCheated", true);
        setResult(Activity.RESULT_OK, data);
    }

    // TODO: Remove before going to production
    @OnClick(R.id.answer) void onReset() {
        if (!BuildConfig.DEBUG) return;

        confirmButton.setVisibility(View.VISIBLE);
        answer.setVisibility(View.INVISIBLE);
    }
}
