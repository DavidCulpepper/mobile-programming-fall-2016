package net.davidculpepper.geoquiz;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {

    public static final int CHEAT_REQUEST = 0xBEEF;
    public static final String TAG = MainActivity.class.getName();

    private Quiz quiz;
    boolean hasCheated;

    @BindView(R.id.questionText) TextView questionText;
    @BindView(R.id.questionCounter) TextView questionCounter;
    @BindView(R.id.previousButton) View previousButton;
    @BindView(R.id.nextButton) View nextButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, "onCreate() " + this.toString());
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        if (savedInstanceState != null) {
            quiz = (Quiz) savedInstanceState.getSerializable("model");
        } else {
            buildQuiz();
        }
        updateQuestion();
    }

    private void buildQuiz() {
        List<Question> questions = new ArrayList<>();
        questions.add(new Question(R.string.sky_blue_question, true, UUID.randomUUID()));
        questions.add(new Question(R.string.sky_blue_question, true, UUID.randomUUID()));
        questions.add(new Question(R.string.sky_blue_question, true, UUID.randomUUID()));
        quiz = new Quiz(
                R.string.correct_response,
                R.string.wrong_response,
                questions);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putSerializable("model", quiz);
    }

    @OnClick(R.id.trueButton)
    public void onTrueClicked() {
        onResponse(true);
    }

    @OnClick(R.id.falseButton)
    public void onFalseClicked() {
        onResponse(false);
    }

    @OnClick(R.id.cheatButton)
    public void onCheatClicked() {
        startActivityForResult(CheatActivity.createIntent(
                this,
                getString(quiz.getCurrentQuestion().getText()),
                quiz.getCurrentQuestion().isCorrect(true)), CHEAT_REQUEST);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case CHEAT_REQUEST:
                hasCheated = CheatActivity.hasCheated(data);
                break;
        }
    }

    @OnClick(R.id.nextButton)
    public void onNextClicked() {
        quiz.goToNextQuestion();
        updateQuestion();
    }

    private void updateQuestion() {
        questionText.setText(quiz.getCurrentQuestion().getText());
        questionCounter.setText(getString(R.string.question_counter_text, quiz.getQuestionNumber(), quiz.getTotalNumberOfQuestions()));

        previousButton.setVisibility(!quiz.isFirstQuestion() ? View.VISIBLE : View.INVISIBLE);
        nextButton.setVisibility(!quiz.isLastQuestion() ? View.VISIBLE : View.INVISIBLE);
    }

    @OnClick(R.id.previousButton)
    public void onPreviousClicked() {
        quiz.goToPreviousQuestion();
        updateQuestion();
    }

    private void onResponse(boolean response) {
        if (quiz.getCurrentQuestion().isCorrect(response)) {
            if (hasCheated) {
                showMessage(R.string.cheated);
            } else {
                showMessage(quiz.getCorrectResponse());
            }
        } else {
            showMessage(quiz.getWrongResponse());
        }
    }

    private void showMessage(int message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }
}
