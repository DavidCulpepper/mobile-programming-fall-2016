**CSC 490 - Mobile Application Development**

**Assignment 3**

**Summary:** Enhance your current application showing a list of memes. You will be adding persistence to your application. You will provide a document detailing the design decisions that you made along with your rationale for those decisions.

**Due:** Thursday, 11/10/2016 at 6:00 PM

**Rubric:** 100 Points

1. (10 pts) When I launch the app, I should see a list of existing memes.
1. (10 pts) When I am on the list, a mechanism should be available for me to start creating a meme.
1. (10 pts) When I am on the list, a mechanism should be available for me to view a closeup of the meme.
1. (10 pts) When I go to create a meme, I want to be able to enter text for the top, the bottom, and provide a URL that points to an image.
1. (5 pts) After I create a meme, I should be returned to the list and I should see my meme added to the list.
1. (15 pts) I want to be able to edit an existing meme, being able to change the top text, bottom text, as well as the url for the image.
1. (20 pts) When I close the app and reopen it, any memes that I have created should be available.
1. (10 pts) Provide a README.md file in the root of your project that details the design decisions that you make, suggests alternatives that you considered, and explains your reasoning for choosing what you did.
1. (10 pts) Impress me.

**Submission:** You will submit your project by adding it to your git repository and pushing it to your BitBucket repository (that you have granted me access) by the due date and time.  Please name the project memes.
