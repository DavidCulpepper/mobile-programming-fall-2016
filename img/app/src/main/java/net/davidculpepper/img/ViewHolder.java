package net.davidculpepper.img;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by dculpepper on 11/8/2016.
 */
public class ViewHolder extends RecyclerView.ViewHolder {

    public TextView topText;
    public TextView bottomText;
    public ImageView image;
    public Meme meme;

    public ViewHolder(View itemView) {
        super(itemView);
        topText = (TextView) itemView.findViewById(R.id.top_text);
        bottomText = (TextView) itemView.findViewById(R.id.bottom_text);
        image = (ImageView) itemView.findViewById(R.id.image);
    }
}
