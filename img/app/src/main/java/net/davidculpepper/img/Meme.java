package net.davidculpepper.img;

import java.io.Serializable;

public class Meme implements Serializable {
    private String topText;
    private String bottomText;
    private String url;

    public Meme(String topText, String bottomText, String url) {
        this.topText = topText;
        this.bottomText = bottomText;
        this.url = url;
    }

    public String getTopText() {
        return topText;
    }

    public void setTopText(String topText) {
        this.topText = topText;
    }

    public String getBottomText() {
        return bottomText;
    }

    public void setBottomText(String bottomText) {
        this.bottomText = bottomText;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
