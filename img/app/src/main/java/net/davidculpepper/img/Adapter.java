package net.davidculpepper.img;

import android.content.Intent;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by dculpepper on 11/8/2016.
 */
public class Adapter extends RecyclerView.Adapter<ViewHolder> {

    private final AppCompatActivity activity;
    private List<Meme> items = new ArrayList<>();

    public Adapter(AppCompatActivity activity) {
        this.activity = activity;
    }

    public void updateList(List<Meme> items) {
        this.items = items;
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.list_item_meme, parent, false);
        final ViewHolder vh = new ViewHolder(view);
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(activity, ViewMemeActivity.class);
                i.putExtra("data", vh.meme);
//                ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(activity, null, null);
//                activity.startActivity(i, options.toBundle());
                activity.startActivity(i);
            }
        });
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Meme m = items.get(position);
        holder.meme = m;
        holder.topText.setText(m.getTopText());
        holder.bottomText.setText(m.getBottomText());
        Glide.with(holder.itemView.getContext()).load(m.getUrl()).into(holder.image);
    }

    @Override
    public int getItemCount() {
        return items.size();
    }
}
