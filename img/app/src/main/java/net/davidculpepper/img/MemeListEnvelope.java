package net.davidculpepper.img;

import java.util.List;

/**
 * Created by dculpepper on 11/8/2016.
 */

public class MemeListEnvelope {
    public final List<Meme> items;

    public MemeListEnvelope(List<Meme> items) {
        this.items = items;
    }
}
