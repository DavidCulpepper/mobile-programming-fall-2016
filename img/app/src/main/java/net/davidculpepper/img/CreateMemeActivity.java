package net.davidculpepper.img;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

public class CreateMemeActivity extends AppCompatActivity {

    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_meme);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.inflateMenu(R.menu.menu_create);
        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.action_save:
                        saveMeme();
                        return true;
                    default:
                        return false;
                }
            }
        });
    }

    private void saveMeme() {
        TextView topText = (TextView) findViewById(R.id.top_text);
        TextView bottomText = (TextView) findViewById(R.id.bottom_text);
        TextView url = (TextView) findViewById(R.id.url);

        Meme m = new Meme(topText.getText().toString(), bottomText.getText().toString(), url.getText().toString());

        Intent i = new Intent();
        i.putExtra("data", m);
        setResult(RESULT_OK, i);
        finish();
    }
}
