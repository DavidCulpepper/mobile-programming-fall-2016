package net.davidculpepper.img;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

public class ViewMemeActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_meme);

        Meme meme = (Meme) getIntent().getSerializableExtra("data");

        ImageView image = (ImageView)findViewById(R.id.image);
        TextView topText = (TextView)findViewById(R.id.top_text);
        TextView bottomText = (TextView)findViewById(R.id.bottom_text);

        Glide.with(this).load(meme.getUrl()).into(image);
        topText.setText(meme.getTopText());
        bottomText.setText(meme.getBottomText());
    }
}
