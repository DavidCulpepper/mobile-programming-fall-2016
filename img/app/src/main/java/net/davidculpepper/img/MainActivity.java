package net.davidculpepper.img;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.View;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private static final int REQUEST_CREATE_MEME = 1000;

    private RecyclerView recyclerView;
    private Adapter adapter;
    private List<Meme> items;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (savedInstanceState != null) {
            items = new Gson().fromJson(savedInstanceState.getString("data"),MemeListEnvelope.class).items;
        } else {
            items = new ArrayList<>();
            items.add(new Meme("Hello", "World", "https://imgflip.com/s/meme/One-Does-Not-Simply.jpg"));
            items.add(new Meme("Hello", "World", "https://imgflip.com/s/meme/One-Does-Not-Simply.jpg"));
            items.add(new Meme("Hello", "World", "https://imgflip.com/s/meme/One-Does-Not-Simply.jpg"));
        }

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                launchCreateMemeActivity();
            }
        });

        adapter = new Adapter(this);
        adapter.updateList(items);

        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(new GridLayoutManager(this, 2));

        int dp = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 8, getResources().getDisplayMetrics());
        recyclerView.addItemDecoration(new GridSpacingItemDecoration(2, dp, false));
        recyclerView.setAdapter(adapter);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("data", new Gson().toJson(new MemeListEnvelope(items)));
    }

    private void launchCreateMemeActivity() {
        Intent i = new Intent(this, CreateMemeActivity.class);
        startActivityForResult(i, REQUEST_CREATE_MEME);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case REQUEST_CREATE_MEME:
                handleCreateMemeResult(resultCode, data);
        }
    }

    private void handleCreateMemeResult(int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            Meme m = (Meme) data.getSerializableExtra("data");
            items.add(m);
            adapter.notifyDataSetChanged();
        }
    }
}