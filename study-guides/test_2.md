1. Fragments
- Lifecyle - onCreateView
- Pass data to a fragment
- setArguments(Bundle) / getArguments()
- Why?

2. Compare/contrast different layouts
- layout_*
- LinearLayout
- RelativeLayout
- FrameLayout
- layout_weight

3. Storage
- SharedPreferences
- SQLite
- Compare / Contrast
- SqliteOpenHelper

4. Web
- CRUDL - REST API
- http access from android
- sample retrofit interface

````
interface Api {
  @GET("/contacts")
  Call<List<Contact>> getContacts();
  
  @GET("/contacts/{id}")
  Call<Contact> getContact(@PATH("id") int id);
}
````

5. Graduate student presentations
