**CSC 490 - Mobile Application Development**

**Test 1**

**Date:** Tuesday, 10/4/2016 at 8:00 PM

**Summary:** This test will cover the basics of git as well as Chapters 1-6 in the book.

#### Basics of git

- Difference between modified, staged, and committed
- Difference between distributed vcs non distributed
- How to initialize repository
- How to stage changes
- How to commit changes
- How to push changes to a remote

#### Chapter 1: Your first android app

- What is a toast. When should you use one?
- What are string resources. Why should you use them

#### Chapter 2: Model View Controller

- Benefits? Rationale?

#### Chapter 3: The Activity Lifecycle

- What you should do at different events
- What the event order is

#### Chapter 4: Debugging Android Apps

- Logging

#### Chapter 5: Your second activity

- Navigate between activities

#### Chapter 6: Android SDK Versions and Compatibility

- AppCompat
- targetSdk, vs min, vs max, vs compile