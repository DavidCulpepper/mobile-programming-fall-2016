package net.davidculpepper.criminalintent;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import java.util.UUID;

public class CreateCrimeFragment extends Fragment {

    private EditText titleText;
    private Button createButton;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_create_crime, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        titleText = (EditText) view.findViewById(R.id.title);
        createButton = (Button) view.findViewById(R.id.create);

        createButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Crime c = new Crime();
                c.setId(UUID.randomUUID());
                c.setTitle(titleText.getText().toString());
                ((MainActivity)getActivity()).showCrimeDetails(c);
            }
        });
    }
}
