package net.davidculpepper.criminalintent;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class CrimeFragment extends Fragment {

    private TextView titleText;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_crime, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        titleText = (TextView) view.findViewById(R.id.title);
        Crime c = (Crime) getArguments().getSerializable("crime");
        titleText.setText(c.getTitle());
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    public static CrimeFragment createInstance(Crime c) {
        Bundle args = new Bundle();
        args.putSerializable("crime", c);

        CrimeFragment fragment = new CrimeFragment();
        fragment.setArguments(args);
        return fragment;
    }
}