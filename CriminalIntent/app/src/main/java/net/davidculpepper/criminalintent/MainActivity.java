package net.davidculpepper.criminalintent;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .replace(android.R.id.content, new CreateCrimeFragment())
                    .commit();
        }
    }

    public void showCrimeDetails(Crime c) {
        getSupportFragmentManager().beginTransaction()
                .replace(android.R.id.content, CrimeFragment.createInstance(c))
                .addToBackStack("create")
                .commit();
    }
}